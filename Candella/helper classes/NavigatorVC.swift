//
//  NavigatorVC.swift
//  masho
//
//  Created by Appzoc on 26/06/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

struct Navigator{

    func getDestination(for url : URL) -> UIViewController?{
        let destination = Destination(for: "\(url)")
        switch destination {
        case .pdtid(let productId)?:
            let destinationVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
            let navigationVC = UINavigationController(rootViewController: destinationVC)
            navigationVC.navigationBar.isHidden = true
            destinationVC.isFrom = .other
            destinationVC.productID = productId
            return navigationVC
        
        case .none:
            return nil
        }
        
    }
}
    enum Destination{
        case pdtid(productId : String)
        init?(for url : String){
            if let urlComponents = URLComponents(string: url),let queryItems = urlComponents.queryItems {
                let name = queryItems[0].name
                let value = queryItems[0].value
                print(name)
                print(value!)
                self = .pdtid(productId: value!)
            }else{
                return nil
            }
        }
    }


