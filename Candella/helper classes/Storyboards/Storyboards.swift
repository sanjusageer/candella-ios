//
//  Storyboards.swift
//  E-expo
//
//  Created by WC-64 on 23/02/21.
//

import Foundation
import UIKit

enum Storyboards: String {
   
    case Login
    case Main
    case Profile
    case Home
    case Cart
    case Category
    case Loyality
    case AdvanceBooking
    

    //MARK: - Storybard instance with Name
    var instance: UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    //MARK: View Controller from Storyboard
    func viewController<T: UIViewController>(controller: T.Type) -> T {
        
        let storyboardID = (controller as UIViewController.Type).storyboardID
        if #available(iOS 13.0, *) {
            return instance.instantiateViewController(identifier: storyboardID) as! T
        } else {
            // Fallback on earlier versions
            return instance.instantiateViewController(withIdentifier: storyboardID) as! T
        }
        
    }
    
    //MARK: - Set initial View Controller
    func initialVC() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
    
}

