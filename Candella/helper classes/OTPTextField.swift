//
//  OTPTextField.swift
//  Noo-gha
//
//  Created by WC-64 on 04/03/21.
//  Copyright © 2021 Noogha. All rights reserved.
//

import UIKit

protocol OTPTFDelegate : UITextFieldDelegate {
    func didTapBackspace(sender: OTPTextField)
}

class OTPTextField : UITextField {
    
    override func deleteBackward() {
        let isTextfieldEmpty: Bool = (self.text?.isEmpty)!
        if isTextfieldEmpty {
            if let delegate = delegate as? OTPTFDelegate {
                delegate.didTapBackspace(sender: self)
            }
        }
        super.deleteBackward()
    }
    
}
