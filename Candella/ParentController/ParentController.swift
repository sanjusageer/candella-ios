//
//  ParentController.swift
//  Dermacy
//
//  Created by WC-64 on 10/07/21.
//

import UIKit

class ParentController: UIViewController {
    
    
    
    //MARK: - PROPERTIES
    //MARK: - VIEW CONTROLLER PROPERTY ANALYSER
    var pushVC: UIViewController? = nil {
        didSet {
            guard let vc = pushVC, let navigationVC = self.navigationController else {return}
            navigationVC.pushViewController(vc, animated: true)
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
