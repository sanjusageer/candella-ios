//
//  PANTextField.swift
//  josco
//
//  Created by WC46 on 07/01/22.
//  Copyright © 2022 Josco. All rights reserved.
//

import Foundation

import UIKit

protocol PANTFDelegate : UITextFieldDelegate {
    func didTapBackspace(sender: PANTextField)
}

class PANTextField : UITextField {
    
    override func deleteBackward() {
        let isTextfieldEmpty: Bool = (self.text?.isEmpty)!
        if isTextfieldEmpty {
            if let delegate = delegate as? PANTFDelegate {
                delegate.didTapBackspace(sender: self)
            }
        }
        super.deleteBackward()
    }
    
}
