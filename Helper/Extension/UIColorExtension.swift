//
//  UIColorExtension.swift
//  Dermacy
//
//  Created by WC-64 on 10/07/21.
//

import UIKit

extension UIColor {
    
    static let theme = UIColor(named: "AccentColor")!
    static let mustardYellow = UIColor(named: "MustardYellow")!
    static let whiteF6F6F6 = UIColor(named: "WhiteF6F6F6")!
    static let themeGray = UIColor(named: "Grey727272")!

}




//extension UIColor {
//    convenience init(hexString: String, alpha: CGFloat = 1.0) {
//        var hexInt: UInt32 = 0
//        let scanner = Scanner(string: hexString)
//        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
//        scanner.scanHexInt32(&hexInt)
//
//        let red = CGFloat((hexInt & 0xff0000) >> 16) / 255.0
//        let green = CGFloat((hexInt & 0xff00) >> 8) / 255.0
//        let blue = CGFloat((hexInt & 0xff) >> 0) / 255.0
//        let alpha = alpha
//
//        self.init(red: red, green: green, blue: blue, alpha: alpha)
//    }
//}

public extension UIColor {
  
  /// Returns color from its hex string
  ///
  /// - Parameter hexString: the color hex string
  /// - Returns: UIColor
    static func fromHex(hexString: String) -> UIColor {
    let hex = hexString.trimmingCharacters(
      in: CharacterSet.alphanumerics.inverted)
    var int = UInt32()
    Scanner(string: hex).scanHexInt32(&int)
    let a, r, g, b: UInt32
    switch hex.count {
    case 3: // RGB (12-bit)
      (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
    case 6: // RGB (24-bit)
      (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
    case 8: // ARGB (32-bit)
      (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
    default:
      return UIColor.clear
    }
    
    return UIColor(
      red: CGFloat(r) / 255,
      green: CGFloat(g) / 255,
      blue: CGFloat(b) / 255,
      alpha: CGFloat(a) / 255)
  }
}
